int value, frag, turnOn = 0;
//Leds que vão indicar o valor do potenciometro
int leds[] = {2,3,4,5};
int size = sizeof(leds)/2;
//Resistência máxima do potenciometro
int ohm = 1024;

void setup() {
  for(int i = 0; i < size;i++){
    pinMode(leds[i], OUTPUT);
  }
}

void loop() {
  //Captura o Valor do potenciometro (vai até 1023, então soma +1
  value = analogRead(0)+1;
  //Calcula a faixa para cada led
  frag = ohm/size;  
  
  //Calcula o numero de leds que devem acender
  turnOn = value/frag;
  //Vetor começa a contar na posição 0
  turnOn = turnOn-1;
  
  //Acende do led 0 até o que deve acender
  for(int i = 0; i<=turnOn;i++){
    digitalWrite(leds[i], HIGH);
  }

  //Apaga outros leds
  for(int j = turnOn+1;j<=size-1;j++){
    digitalWrite(leds[j], LOW);
  }
}