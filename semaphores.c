//Estrutura de um semaforo, semaforos amarelos possuem 0 na variavel yellowPin
struct Semaphore{
  int greenPin;
  int yellowPin;
  int redPin; 
};

int size = 4;

//Define uma array de semanforos
Semaphore semaphores[] = {
  {3,2,1},
  {6,5,4},
  {8,0,7},
  {10,0,9},
};

void setup() {
  for(int i = 0; i<size;i++){
    pinMode(semaphores[i].greenPin, OUTPUT);
    pinMode(semaphores[i].yellowPin, OUTPUT);
    pinMode(semaphores[i].redPin, OUTPUT);
  }
}

void loop() {
  for(int i = 0; i<size/2;i++){
    digitalWrite(semaphores[i].greenPin, HIGH);
    digitalWrite(semaphores[i+2].greenPin, HIGH);
    
    for(int j = 0; j<size;j++){
      if((i != j) && (j != (i+2))){
        digitalWrite(semaphores[j].redPin, HIGH);
      }
    }
    
    delay(3500);
    digitalWrite(semaphores[i].greenPin, LOW);
    digitalWrite(semaphores[i+2].greenPin, LOW);
      
    digitalWrite(semaphores[i].yellowPin, HIGH);
    delay(500);
    digitalWrite(semaphores[i].yellowPin, LOW);
    
    for(int j = 0; j<size;j++){
      digitalWrite(semaphores[j].redPin, LOW); 
    }
  }
}